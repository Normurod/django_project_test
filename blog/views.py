from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import datetime

# Create your views here.

def home(request):

    return render(request, 'blog/home.html', {})


@csrf_exempt
def comment(request):

    now = datetime.datetime.now()
    formatedDate = now.strftime("%d-%m-%Y")
    res = "%s %s" % (request.POST['comment'], formatedDate)
    return HttpResponse(res, content_type="text/plain")